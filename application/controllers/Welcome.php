<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->helper('url');
		$this->_view('', null);
	}

	private function _view($viewfile, $data) {
		// View logic specific to this controller
		$this->load->view('common/header');
		$this->load->view('nav/top_nav');
		$this->load->view('common/container_start');
		$this->load->view($viewfile, $data);
		$this->load->view('common/container_end');
		$this->load->view('common/footer');
	}

}

/* End of file Welcome.php */
/* Location: ./application/controllers/Welcome.php */