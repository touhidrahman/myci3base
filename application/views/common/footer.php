
	<!-- JavaScripts
	================================================== 
	Placed at the end of the document so the pages load faster -->
	<script src="<?php echo base_url('public/jquery/jquery-2.1.4.min.js');?>"></script>
	<script src="<?php echo base_url('public/bootstrap/js/bootstrap.min.js');?>"></script>
	<script src="<?php echo base_url('public/bootstrap/js/docs.min.js');?>"></script>
	<script src="<?php echo base_url('public/custom/app.js');?>"></script>
</body>
</html>