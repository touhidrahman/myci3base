<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->helper('language');
		// Load language file
		$this->lang->load('en_admin', 'english');
	}


}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */